<?php

class BuscaDadosFiscal extends Read{
    private $Nome;
    private $Dados;

    function setNome($nome){
        $this->Nome = (string) $nome;
    }

    function getNome(){
        return $this->Nome;
    }

    function setDados($dados){
        $this->Dados = $dados;
    }

    function getDados(){
        return $this->Dados;
    }

    function buscaDados($nome){
        self::setNome($nome);
        parent::ExeRead('tbl_Fiscal','WHERE strNomeFiscal = :fiscal' ,'fiscal='.self::getNome());
        self::setDados(parent::getResult());
        return self::getDados();
    }
}

?>
