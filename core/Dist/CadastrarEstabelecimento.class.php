<?php

class CadastrarEstabelecimento extends Create{

    private $CNPJ;
    private $RazaoSocial;
    private $NomeFantasia;
    private $CMC;
    private $AreaAtuacao;
    private $Logradouro;
    private $Numero;
    private $Complemento = null;
    private $Bairro;
    private $Cidade = 'Ubatuba';
    private $Result;

    function __construct($CNPJ, $RazaoSocial, $NomeFantasia, $CMC, $AreaAtuacao, $Logradouro, $Numero, $Complemento, $Bairro, $Cidade){
        self::setCNPJ($CNPJ);
        self::setRazaoSocial($RazaoSocial);
        self::setNomeFantasia($NomeFantasia);
        self::setCMC($CMC);
        self::setAreaAtuacao($AreaAtuacao);
        self::setLogradouro($Logradouro);
        self::setNumero($Numero);
        self::setComplemento($Complemento);
        self::setBairro($Bairro);
        self::setCidade($Cidade);

        self::NovoEstabelecimento();
    }

    function NovoEstabelecimento(){
        $dados = ['CNPJ' => self::getCNPJ(), 'strRazaoSocial' => self::getRazaoSocial(), 'strNomeFantasia' => self::getNomeFantasia(), 'strCMC' => self::getCMC(), 'strAtividade' => self::getAreaAtuacao(), 'strLogradouro' => self::getLogradouro(), 'strNumero' => self::getNumero(), 'strComplemento' => self::getComplemento(), 'strBairro' => self::getBairro(), 'strCidade' => self::getCidade()];
        self::setResult(parent::ExeCreate('tbl_Estabelecimento', $dados));
    }

    function setCNPJ($CNPJ){
        $this->CNPJ = $CNPJ;
    }

    function setRazaoSocial($RazaoSocial){
        $this->RazaoSocial = (string) $RazaoSocial;
    }

    function setNomeFantasia($NomeFantasia){
        $this->NomeFantasia = (string) $NomeFantasia;
    }

    function setCMC($CMC){
        $this->CMC = $CMC;
    }

    function setAreaAtuacao($AreaAtuacao){
        $this->AreaAtuacao = (string) $AreaAtuacao;
    }

    function setResult($result){
        $this->Result = $result;
    }

    function getResult() {
        return $this->Result;
    }

    function getCNPJ() {
        return $this->CNPJ;
    }

    function getRazaoSocial() {
        return $this->RazaoSocial;
    }

    function getNomeFantasia() {
        return $this->NomeFantasia;
    }

    function getCMC() {
        return $this->CMC;
    }

    function getAreaAtuacao() {
        return $this->AreaAtuacao;
    }

    function setLogradouro($Logradouro){
        $this->Logradouro = (string) $Logradouro;
    }

    function setNumero($Numero){
        $this->Numero = (string) $Numero;
    }

    function setComplemento($Complemento){
        $this->Complemento = (string) $Complemento;
    }

    function setBairro($Bairro){
        $this->Bairro = (string) $Bairro;
    }

    function setCidade($Cidade){
        $this->Cidade = (string) $Cidade;
    }

    function getLogradouro(){
        return $this->Logradouro;
    }

    function getNumero(){
        return $this->Numero;
    }

    function getComplemento(){
        return $this->Complemento;
    }

    function getBairro(){
        return $this->Bairro;
    }

    function getCidade(){
        return $this->Cidade;
    }


}

?>
