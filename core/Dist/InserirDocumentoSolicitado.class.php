<?php

class InserirDocumentoSolicitado extends Create{

    private $Descricao;
    private $idTermo;
    private $Tabela;
    private $Result;

    function __construct($Tabela, $Dados, $IdTermo){
        self::setTabela($Tabela);
        self::setDescricao($Dados);
        self::setIdTermo($IdTermo);
        self::Cadastrar();
    }

    function Cadastrar(){
        $dados = self::getDescricao();
        foreach($dados as $key => $value){
            if($value != null){
                parent::ExeCreate(self::getTabela(), ['strDescricao' => $value, 'idDocumento' => self::getIdTermo()]);
                self::setResult(parent::getResult());
            }
        }
    }

    function getDescricao() {
        return $this->Descricao;
    }

    function getTabela() {
        return $this->Tabela;
    }

    function getResult() {
        return $this->Result;
    }

    function getIdTermo() {
        return $this->idTermo;
    }

    function setIdTermo($idTermo) {
        $this->idTermo = $idTermo;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }

    function setTabela($Tabela) {
        $this->Tabela = $Tabela;
    }

    function setDescricao($Descricao) {
        $this->Descricao = explode("<>", $Descricao);
    }

}

?>
