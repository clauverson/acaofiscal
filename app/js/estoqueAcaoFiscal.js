$(document).ready(function(){

    $('#lista-de-acoes a').click(function(){
        var idAcao = $(this).attr('id');
        var dados = "acao=carregarTIAF&idAcao="+idAcao;
        $.ajax({
            method: "POST",
            url: "app/php/termos/carregarTIAF.ajx.php",
            datatype: 'json',
            data: dados,
            success: function(retorno){
                if(retorno == 0){
                    $('#controle-acao-fiscal').load("app/php/termos/tiaf.php?acao=novoTIAF");
                } else {
                    $('#controle-acao-fiscal').load("app/php/controlarAcaoFiscal.php?idAcao="+idAcao);
                }
            }
        });

    });

});
