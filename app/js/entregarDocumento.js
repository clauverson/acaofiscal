$(document).ready(function(){

    var selectDocSolicitado = "";

    $.ajax({
        method: "POST",
        url: "app/php/entregarDocumento.ajx.php",
        datatype: 'json',
        data: "acao=DocumentosSolicitados",
        success: function(retorno){
            selectDocSolicitado = retorno;
        }
    });

    var upload = $("#EnviarArquivos").uploadFile({
        url:"app/php/upload-documentos.php",
        fileName:"myfile",
        extraHTML:function(){
            var html = selectDocSolicitado+"<div class='col s12' style='margin-top: 1em;'><b>Períodos e/ou observações:</b><input type='text' name='tags' value='' placeholder='Adicione informações ao documento'/></div>";
            html += "<script>$('.selectpicker').selectpicker({style: 'btn-primary',size: 10});</script>";
            return html;
        },
        dynamicFormData: function(){
            var data = {"documento":""};
            return data;
        },
        autoSubmit:false
    });


    $(".ajax-upload-dragdrop").removeAttr('style');

    $('#enviarDocumentos').click(function(){
        upload.startUpload();
    });
});
