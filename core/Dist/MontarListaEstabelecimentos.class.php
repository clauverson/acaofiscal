<?php

class MontarListaEstabelecimentos{
    private $Locais;
    private $Template;
    private $Result;

    function __construct($listaLocais, $template){
        self::setLocais($listaLocais);
        self::setTemplate($template);
        self::montarLista();
    }

    function setLocais($locais){
        $this->Locais = $locais;
    }

    function setTemplate($modeloItens){
        $this->Template = $modeloItens;
    }
    function setResult($valor){
        $this->Result = $valor;
    }

    function getLocais(){
        return $this->Locais;
    }

    function getTemplate(){
        return $this->Template;
    }

    function getResult(){
        return $this->Result;
    }

    function montarLista(){
        $locais = self::getLocais();
        $tela = self::getTemplate();
        $result = "";

        foreach($locais as $estabelecimento){
            $tela->NomeFantasia = $estabelecimento['strNomeFantasia'];
            $tela->CNPJ = $estabelecimento['CNPJ'];
            $tela->ID = $estabelecimento['id'];
            $result .= $tela->pegar();
        }

        self::setResult($result);
    }

}



?>
