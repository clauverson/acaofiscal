$(document).ready(function(){

    // carregar lista de acoes abertas
    $.ajax({
        method: "POST",
        url: "app/php/gerenciarAcaoFiscal.ajx.php",
        datatype: 'json',
        data: "acao=listarAcoesAbertas",
        success: function(retorno){
            $('#tabelaAcoesAbertas').html(retorno);
            $('.btnAcessarAcoesAbertas').click(function(){
                $('#conteudoPainel').load("app/php/controlarAcaoFiscal.php?idAcao="+$(this).attr('id'));
            });
        }
    });

    $('select').material_select();

    $('#menu_configuracoes').click(function(){
        $('#bt-menu').addClass('bt-menu-open');
    });

    $('.bt-overlay').click(function(){
        $('#bt-menu').removeClass('bt-menu-open');
    });

    $('#config-tabela').click(function(){
        exibirModal();
    });

    function exibirModal(){
        var modal = UIkit.modal("#modal");
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    }

});
