<?php

class BuscarToleranciaPorId extends Read{

    private $idAcao;
    private $Result;

    function __construct($idAcao){
        self::setIdAcao($idAcao);
        self::BuscarDados();
    }

    private function BuscarDados(){
        parent::ExeRead('tbl_Tolerancia','WHERE idAcaoFiscal = :idAcao' ,'idAcao='.self::getIdAcao());
        self::setDados(parent::getResult());
    }

    function getResult() {
        return $this->Result;
    }

    function getIdAcao() {
        return $this->idAcao;
    }

    function setIdAcao($idAcao) {
        $this->idAcao = $idAcao;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }
}

?>
