$(document).ready(function(){

    $('#editar-termo').load("app/php/imprimir_termo.php?tipoDocumento=TIAF");

    $('.collapsible').collapsible();

    var circularSlider = $('#slider').CircularSlider({
        min : 2,
        max: 15,
        value : 2,
        labelSuffix: " dias"
    });

    $("input[type='checkbox']").candlestick({
        // for on value
        'on': 'Sim',
        // for off value
        'off': 'Não',
        // for non value
        'default': 'vazio',
        // enable touch swipe
        'swipe': true,
        // callbacks
        afterAction: function() {},
        afterRendering: function() {},
        afterOrganization: function() {},
        afterSetting: function() {}
    });


});
