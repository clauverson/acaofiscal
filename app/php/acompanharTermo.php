<?php

if(!session_id()){
    session_start();
}

include_once('../../core/Config.inc.php');

$caminho = RAIZ . "app/html/acompanharTermo.html";

$_SESSION['nomeAto'] = new ViewComParametro('viewDocsIniciados', "WHERE idAcao = ".$_SESSION['idAcao']);
$_SESSION['nomeAto'] = $_SESSION['nomeAto']->getResult()[$_REQUEST['indiceAto']]['Ato'];

$tpl = new Template($caminho);

$dadosAcao = new BuscarDadosAcao($_SESSION['idAcao']);
$numeroAcao = $dadosAcao->getResult()[0]['intNumeroAcaoFiscal'] .' / '. $dadosAcao->getResult()[0]['intAnoAcaoFiscal'];
$numeroCompletoAcao = $dadosAcao->getResult()[0]['chSigla'].' '. $numeroAcao;
$tpl->NumeroAcaoFiscalCompleto = $numeroCompletoAcao;

$tpl->TipoDocumento = $_SESSION['nomeAto'];

//Procedimentos

$conteudo = new ViewComParametro('ViewListarServicos', "WHERE nomeAto = '".$_SESSION['nomeAto']."'");
$templateProcedimentos = "";
foreach($conteudo->getDados() as $key => $value){
    $view = new ViewComParametro($value['strNomeView'], "where strNomeBiblioteca ='".$value['strNomeBiblioteca']."';");

    $caminhoPlugin = RAIZ . "app/html/bibliotecas/".$value['strNomeBiblioteca'].".html";
    $plugin = new Template($caminhoPlugin);

    $contador = 0;
    foreach($view->getDados()[0] as $chave => $valor){
        if($contador != 0){
            $plugin->$chave = $valor;
        } else {
            $contador = 1;
        }
    }

    $urlJS = "app/js/bibliotecas/".$value['strNomeBiblioteca'].".js";
    $templateProcedimentos .= "<script src='".$urlJS."'></script>";
    $templateProcedimentos .= $plugin->pegar();
}

$tpl->ListaDeProcedimentos = $templateProcedimentos;

//Fim procedimentos

$tpl->show();

?>
