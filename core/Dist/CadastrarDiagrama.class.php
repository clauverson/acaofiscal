<?php

class CadastrarDiagrama extends Create{

    private $Result = array();
    private $Atos;
    private $Links;

    function __construct($atos, $links){
        self::setAtos($atos);
        self::setLinks($links);

        self::GravarAto();
    }

    function GravarAto(){
        $atos = self::getAtos();
        $links = self::getLinks();

        foreach($atos as $key => $value){
            $dados = ['strNomeAto' => $value['nome'], 'intPosX' => $value['posicaox'], 'intPosY' => $value['posicaoy']];
            parent::ExeCreate("tbl_MapaAtos", $dados);
            $atos[$key]['idBanco'] = parent::getResult();
            self::setResult(parent::getResult());
        }

        if((sizeof(self::getResult())) == (sizeof($atos))){
            foreach($links as $key => $value){
                $inicio = $atos[$value['inicio']]['idBanco'];
                $fim = $atos[$value['fim']]['idBanco'];
                $vertices = json_encode($value['vertices']);

                $dados = ['idInicio' => $inicio, 'idFim' => $fim, 'strPosicaoCurva' => $vertices, 'intLiberdadeCaminho' => $value['caminho']];
                parent::ExeCreate("tbl_MapaAtos", $dados);
            }
        }
    }

    function getAtos(){
        return $this->Atos;
    }

    function getResult(){
        return $this->Result;
    }

    function getLinks(){
        return $this->Links;
    }

    function setLinks($links){
        $this->Links = $links;
    }

    function setResult($result){
        array_push($this->Result, $result);
    }

    function setAtos($atos){
        $this->Atos = $atos;
    }

}

?>
