<?php

class LimparTabela extends Delete{

    private $Tabela;
    private $Parametros;
    private $Result;

    function __construct($NomeTabela, $parametros){
        self::setTabela($NomeTabela);
        self::Limpar();
    }

    function Limpar(){
        $tabela = self::getTabela();
        $parametros = self::getParametros();

        parent::ExeDelete($tabela, $parametros, '');

    }

    function getTabela() {
        return $this->Tabela;
    }

    function getResult() {
        return $this->Result;
    }

    function getParametros() {
        return $this->Parametros;
    }

    function setParametros($parametros) {
        $this->Parametros = $parametros;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }

    function setTabela($Tabela) {
        $this->Tabela = $Tabela;
    }

}

?>
