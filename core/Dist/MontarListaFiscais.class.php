<?php

class MontarListaFiscais{
    private $Fiscais;
    private $Template;
    private $Result;

    function __construct($listaFiscais, $template){
        self::setFiscais($listaFiscais);
        self::setTemplate($template);
        self::montarAvatar();
    }

    function setFiscais($fiscais){
        $this->Fiscais = $fiscais;
    }

    function setTemplate($modeloAvatar){
        $this->Template = $modeloAvatar;
    }
    function setResult($valor){
        $this->Result = $valor;
    }

    function getFiscais(){
        return $this->Fiscais;
    }

    function getTemplate(){
        return $this->Template;
    }

    function getResult(){
        return $this->Result;
    }

    function montarAvatar(){
        $fiscais = self::getFiscais();
        $tela = self::getTemplate();
        $result = "";

        foreach($fiscais as $pessoa){
            $tela->NomeFiscal = $pessoa['strNomeFiscal'];
            $tela->EmailFiscal = $pessoa['strEmail'];
            $tela->ClassID = $pessoa['id'];
            $result .= $tela->pegar();
        }

        self::setResult($result);
    }

}



?>
