<?php

class RelacionarTermoAcao extends Create{

    function __construct($idAcao, $nomeTermo){
        $dados = ['idAcaoFiscal' => $idAcao, 'nomeAto' => $nomeTermo, 'dtInicio' => date('Y/m/d')];
        parent::ExeCreate("rel_Acao_Ato", $dados);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->Result;
    }

    function setResult($dados){
        $this->Result = $dados;
    }

}

?>
