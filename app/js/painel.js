$(document).ready(function(){

    function mudarCorMenu(){
        $('header nav').css('background-color','#1C88A1');
    }

    $('#conteudoPainel').load("app/php/gerenciarAcaoFiscal.php");

    $('#pagina_inicial').click(function(){
        mudarCorMenu();
        $('#conteudoPainel').load("app/php/gerenciarAcaoFiscal.php");
    });

    $('#menu_fiscalizar').click(function(){
        mudarCorMenu();
        $('#conteudoPainel').load("app/php/criarAcaoFiscal.php");
    });

    $('#menu_acaoFiscal').click(function(){
        mudarCorMenu();
        $('#conteudoPainel').load("app/php/gerenciarAcaoFiscal.php");
    });

    $('#menu_documentos').click(function(){
        mudarCorMenu();
        $('#conteudoPainel').load("app/php/configurarTermos.php");
    });

    $('#menu_painelControle').click(function(){
        $('header nav').css('background-color','#D04528');
        $('#conteudoPainel').load("app/php/painelControle.php");
    });


    // FUNCOES

    // Apenas verifica se os componentes com a classe 'verificar' não estao em branco.
    $.fn.validar = function(componente)
    {
        var valido = true;

        $(componente).each(function(){
            if(!$(this).val()){
                valido = false;
            }
        });

        if(!valido)
            return false;
        else
            return true;
    }

    //emite um alerta de acordo com o tipo informado
    $.fn.mensagem = function(tipo, mensagem){
        Lobibox.notify(tipo, {
            position: 'bottom left',
            size: 'mini',
            msg: mensagem
        });
    };

    $.fn.exibirModal = function(){
        var modal = UIkit.modal("#modal");
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    }

    $.fn.termos = function(acao, nomeTermo, nomeBotao){
        $('#'+nomeBotao).click(function(){
            var dados = $('#itens'+nomeTermo).serialize();

//            if(nomeTermo == "TIAF"){
//                var documentosSolicitados = null;
//                $('#card-DocSolicitado .edit').each(function(){
//                    if(documentosSolicitados != null){
//                        documentosSolicitados += "<>" + $(this).text();
//                    } else {
//                        documentosSolicitados = $(this).text();
//                    }
//                });
//                dados += "&documentosSolicitados="+documentosSolicitados;
//            } else if(nomeTermo == "TIF"){
//
//            }

            dados += "&acao="+acao+"&nomeTermo="+nomeTermo;

            $.ajax({
                method: "POST",
                url: "app/php/configurarTermos.ajx.php",
                datatype: 'json',
                data: dados,
                success: function(retorno){
                    alert(retorno);
                    //                    $('#conteudoPainel').html(retorno);
                    $.fn.mensagem('success', 'Documento atualizado.');
                }
            });
        });
    };

});
