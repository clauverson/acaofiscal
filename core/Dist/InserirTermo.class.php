<?php

class InserirTermo extends Create{

    private $idAcao;
    private $NomeDoc;
    private $Result;
    private $Campos;

    function __construct($nomeDoc, $Campos, $idAcao){
        self::setNomeDoc($nomeDoc);
        self::setCampos($Campos);
        self::setIdAcao($idAcao);

        self::InserirDocumento();
    }

    private function InserirDocumento(){
        $NomeDoc = self::getNomeDoc();
        $dados = "";

        switch($NomeDoc){
            case "TIAF":
                $dados = ['strNomeDoc' => self::getNomeDoc(), 'txtObjetivo' => self::getCampos()['objetivo'], 'txtRegimeEspecialFiscalizacao' => self::getCampos()['regimeEspecial'], 'txtPenas' => self::getCampos()['penas'], 'txtPrazo' => self::getCampos()['prazo'], 'txtObservacoes' => self::getCampos()['observacao'], 'idAcaoFiscal' => self::getIdAcao()];
                break;
            case "TIF":
                $dados = ['strNomeDoc' => self::getNomeDoc(), 'txtPenas' => self::getCampos()['penas'], 'txtPrazo' => self::getCampos()['prazo'], 'txtObservacoes' => self::getCampos()['observacao'], 'idAcaoFiscal' => self::getIdAcao()];
                break;
            case "TD":
                $dados = ['strNomeDoc' => self::getNomeDoc(), 'txtPenas' => self::getCampos()['penas'], 'txtPrazo' => self::getCampos()['prazo'], 'txtObservacoes' => self::getCampos()['observacao'], 'idAcaoFiscal' => self::getIdAcao()];
                break;
                case "AIIM":
                $dados = ['strNomeDoc' => self::getNomeDoc(), 'txtPenas' => self::getCampos()['penas'], 'txtPrazo' => self::getCampos()['prazo'], 'txtObservacoes' => self::getCampos()['observacao'], 'idAcaoFiscal' => self::getIdAcao()];
                break;
        }

        parent::ExeCreate("tbl_Documentos", $dados);
        self::setResult(parent::getResult());
    }

    function getResult() {
        return $this->Result;
    }

    function getNomeDoc() {
        return $this->NomeDoc;
    }

    function getIdAcao() {
        return $this->idAcao;
    }

    function getCampos() {
        return $this->Campos;
    }

    function setCampos($Campos) {
        $this->Campos = $Campos;
    }

    function setIdAcao($idAcao) {
        $this->idAcao = $idAcao;
    }

    function setNomeDoc($NomeDoc) {
        $this->NomeDoc = $NomeDoc;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }
}

?>
