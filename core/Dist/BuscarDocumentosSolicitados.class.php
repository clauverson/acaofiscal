<?php

class BuscarDocumentosSolicitados extends Read{
    private $result;

    function __construct($tabela, $idDoc){
        self::BuscarDocumento($tabela, $idDoc);
    }

    private function BuscarDocumento($tabela, $idDoc){
        if($tabela == "ViewDocumentosPendentes"){

            parent::ExeRead($tabela,'WHERE idAcaoFiscal = :doc' ,'doc='.$idDoc);
            $this->result = parent::getResult();

        } else {
            if($idDoc){
                parent::ExeRead($tabela,'WHERE idDocumento = :doc' ,'doc='.$idDoc);
            }else{
                parent::ExeRead($tabela,'WHERE idDocumento is null');
            }
            if(sizeof(parent::getResult()) > 0){
                $this->result = parent::getResult();
            } else {
                $this->result = null;
            }
        }
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }

}

?>
