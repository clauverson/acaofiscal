<?php

//if(!isset($_SESSION))
//	session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch($acao){
	case "NovaAcao":
		$Check = new Check;

		if(isset($_REQUEST['icms'])){
			$icms = true;
		} else {
			$icms = false;
		}

		if(isset($_REQUEST['iss'])){
			$iss = true;
		} else {
			$iss = false;
		}

		// VERIFICA SE AS DATAS FORAM INFORMADAS
		if((isset($_REQUEST['data-inicio-acao'])) && (isset($_REQUEST['prazo-dias'])) && (is_numeric($_REQUEST['prazo-dias']))){
			$dataInicioAcao = implode("/",array_reverse(explode("/",$_REQUEST['data-inicio-acao'])));
			$prazoDias = $_REQUEST['prazo-dias'];
			$dataFinal = $_REQUEST['data-final'];
		} else {
			echo 'erro-dataInicioAcao'; exit();
		}

		// PERIODOS
		if((isset($_REQUEST['inicio-periodo'])) && (isset($_REQUEST['fim-periodo']))){
			$inicioPeriodo = $_REQUEST['inicio-periodo'];
			$fimPeriodo = $_REQUEST['fim-periodo'];
		} else {
			echo 'erro-periodo'; exit();
		}

		if(isset($_REQUEST['motivacao-principal'])){
			$motivacaoPrincipal = $_REQUEST['motivacao-principal'];
		} else {
			echo 'erro-motivacao'; exit();
		}

		$observacao = $_REQUEST['observacao'];

		// NOME E NUMERO DO DOCUMENTO INTERNO
		if((isset($_REQUEST['nome-documento-interno'])) && (isset($_REQUEST['numero-documento-interno']))){
			$nomeDocumentoInterno = $_REQUEST['nome-documento-interno'];
			$numeroDocumentoInterno = $_REQUEST['numero-documento-interno'];
			$processoInicial = $nomeDocumentoInterno.'/'.$numeroDocumentoInterno;
		} else {
			echo 'erro-processo'; exit();
		}

		// CONTATO DA EMPRESA PARA ESTA ACAO FISCAL
		if((isset($_REQUEST['cpf-autuado'])) && (isset($_REQUEST['nome-autuado'])) && (isset($_REQUEST['telefone-autuado'])) && (isset($_REQUEST['email-autuado']))){
			$cpfAutuado = $_REQUEST['cpf-autuado'];
			$nomeAutuado = $_REQUEST['nome-autuado'];
			$telefoneAutuado = $_REQUEST['telefone-autuado'];
			$emailAutuado = $_REQUEST['email-autuado'];
		} else {
			echo 'erro-contatoEmpresa'; exit();
		}

		// DATA DE CIENCIA DO AUTUADO
		if((isset($_REQUEST['data-ciencia'])) && (isset($_REQUEST['hora-ciencia']))){
			$dataCiencia = $_REQUEST['data-ciencia'];
			$dataCiencia = implode("/",array_reverse(explode("/", $dataCiencia)));
			$horaCiencia = $_REQUEST['hora-ciencia'];
		} else {
			echo 'erro-ciencia'; exit();
		}

		// NOME E NUMERO DO DOCUMENTO QUE DEU INICIO A ACAO FISCAL
		if((isset($_REQUEST['nome-documento-inicio'])) && (isset($_REQUEST['numero-documento-inicio']))){
			$nomeDocumentoInicial = $_REQUEST['nome-documento-inicio'];
			$numeroDocumentoInicial = $_REQUEST['numero-documento-inicio'];
		} else {
			echo 'erro-documentoInicio';
		}


		if(isset($_REQUEST['id-estabelecimento'])){
			$idEstabelecimento = $_REQUEST['id-estabelecimento'];
		} else {
			echo 'erro-IDEmpresa'; exit();
		}

		$idParticipantes = [];
		if(isset($_REQUEST['participantes'])){
			// BUSCA DADOS DO FISCAL DE ACORDO COM O NOME INFORMADO
			$infoFiscal = new BuscaDadosFiscal;
			$participantes = $_REQUEST['participantes'];
			$participantes = explode(',', $participantes);

			// GUARDA O ID DE ACORDO COM O NOME
			foreach($participantes as $nome){
				array_push($idParticipantes, $infoFiscal->buscaDados($nome)[0]['id']);
			}

		} else {
			echo 'erro-participantes'; exit();
		}

		$NumeroAcao = new BuscaNumeroProximaAcao;
		if($NumeroAcao->getProximoNumero()){
			$proximo = ($NumeroAcao->getProximoNumero()[0]['intNumeroAcaoFiscal'])+1;
		} else {
			$proximo = 1;
		}

		$divisao = explode(' ', $participantes[0]);
		$sigla = $divisao[0][0];
		$sigla .= $divisao[1][0];

		$NovaAcao = new NovaAcaoFiscal($sigla, $icms, $iss, $dataInicioAcao, $prazoDias, $dataFinal, $inicioPeriodo, $fimPeriodo, $motivacaoPrincipal, $observacao, $nomeDocumentoInterno, $numeroDocumentoInterno, $processoInicial, $dataCiencia, $horaCiencia, $idEstabelecimento, $idParticipantes, $proximo, $cpfAutuado, $nomeAutuado, $telefoneAutuado, $emailAutuado);

		break;
}

?>
