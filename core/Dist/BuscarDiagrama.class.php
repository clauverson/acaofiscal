<?php

class BuscarDiagrama extends Read{

    private $Atos;
    private $Links;

    function __construct(){
        self::CarregarAtos();
        self::CarregarLinks();
    }

    private function CarregarAtos(){
        parent::ExeRead('tbl_MapaAtos','WHERE strNomeAto is not null');
        self::setAtos(parent::getResult());
    }

    private function CarregarLinks(){
        parent::ExeRead('tbl_MapaAtos','WHERE strNomeAto is null');
        self::setLinks(parent::getResult());
    }

    function getAtos(){
        return $this->Atos;
    }

    function getLinks(){
        return $this->Links;
    }

    function setLinks($links){
        $this->Links = $links;
    }

    function setAtos($atos){
        $this->Atos = $atos;
    }
}

?>
