<?php

class BuscaNumeroProximaAcao extends Read{
    private $proximoNumero;

    function __construct(){
        parent::FullRead("SELECT * FROM tbl_AcaoFiscal WHERE intAnoAcaoFiscal = ".date('Y')." ORDER BY intNumeroAcaoFiscal DESC;");
        $this->proximoNumero = parent::getResult();
        return $this->proximoNumero;
    }

    function getProximoNumero(){
        return $this->proximoNumero;
    }
}

?>
