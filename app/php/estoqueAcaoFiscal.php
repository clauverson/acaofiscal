<?php

if(!session_id()){
    session_start();
}

include_once('../../core/Config.inc.php');

$caminho = RAIZ . "app/html/estoqueAcaoFiscal.html";
$tpl = new Template($caminho);

$acaoFiscal = new ListarAcoesAbertas;

$result = "<div class='cl-effect-20' id='cl-effect-20'>";
foreach($acaoFiscal->getDados() as $nome){
    $result .= "<a id='{$nome['id']}'><span data-hover='Visualizar'>{$nome['strNomeFantasia']}</span></a>";
}
$result .= "</div>";

$tpl->ItensEstoque = $result;


$tpl->show();

?>
