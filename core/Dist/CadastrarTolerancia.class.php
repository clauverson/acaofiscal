<?php

class CadastrarTolerancia extends Create{

    private $Result;
    private $idAcao;

    function __construct($idAcao = null){
        self::setIdAcao($idAcao);
        self::gravarTolerancia();
    }

    private function gravarTolerancia(){
        if(self::getIdAcao() != null){
            $dados = ['idAcaoFiscal' => self::getIdAcao()];
        } else {
            $dados = ['intOrdemPadrao' => '0'];
        }
        parent::ExeCreate("tbl_Tolerancia", $dados);
        self::setResult(parent::getResult());
    }

    public function getResult(){
        return self::Result;
    }

    public function getIdAcao(){
        return self::idAcao;
    }

    public function setIdAcao($idAcao){
        self::idAcao = $idAcao;
    }

    public function setResult($resultado){
        self::Result = $resultado;
    }
}

?>
