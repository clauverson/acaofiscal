<?php

class AtualizarTermo extends Update{

    private $idTermo;
    private $NomeDoc;
    private $Result;
    private $Campos;

    function __construct($nomeDoc, $Campos, $idTermo){
        self::setNomeDoc($nomeDoc);
        self::setCampos($Campos);
        self::setIdTermo($idTermo);

        self::AtualizarDocumento();
    }

    private function AtualizarDocumento(){
        $NomeDoc = self::getNomeDoc();
        $dados = "";

        switch($NomeDoc){
            case "TIAF":
                $dados = ['txtObjetivo' => self::getCampos()['objetivo'], 'txtNotificacao' => self::getCampos()['notificacao'], 'txtRegimeEspecialFiscalizacao' => self::getCampos()['regimeEspecial'], 'txtPenas' => self::getCampos()['penas'], 'txtPrazo' => self::getCampos()['prazo'], 'txtObservacoes' => self::getCampos()['observacao']];
        }

        parent::ExeUpdate("tbl_Documentos", $dados, "WHERE id = :idTermo", "idTermo=".self::getIdTermo());

        self::setResult(parent::getResult());
    }

    function getResult() {
        return $this->Result;
    }

    function getNomeDoc() {
        return $this->NomeDoc;
    }

    function getIdTermo() {
        return $this->idTermo;
    }

    function getCampos() {
        return $this->Campos;
    }

    function setCampos($Campos) {
        $this->Campos = $Campos;
    }

    function setIdTermo($idTermo) {
        $this->idTermo = $idTermo;
    }

    function setNomeDoc($NomeDoc) {
        $this->NomeDoc = $NomeDoc;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }

}

?>
