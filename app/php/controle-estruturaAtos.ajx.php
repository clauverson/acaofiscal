<?php

if(!isset($_SESSION))
    session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch($acao){
    case "CadastrarDiagrama":
        $componentes = json_decode($_REQUEST['dados']);

        $pontos = array();
        $links = array();

        // separa os atos dos links
        foreach($componentes as $key => $value){
            switch($value->type){
                case "fsa.StartState":
                    array_push($pontos, array("id" => $value->id, "nome" => "start", "posicaox" => $value->position->x, "posicaoy" => $value->position->y));
                    break;
                case "fsa.State":
                    array_push($pontos, array("id" => $value->id, "nome" => $value->attrs->text->text, "posicaox" => $value->position->x, "posicaoy" => $value->position->y));
                    break;
                case "fsa.Arrow":
                    array_push($links, array("inicio" => $value->source->id, "fim" => $value->target->id, "caminho" => $value->labels[0]->attrs->text->text, "vertices" => $value->vertices));
                    break;
            }
        }

        // Altera o id no inicio e fim pela posicao do ato no array
        foreach($links as $indice => $valor){
            foreach($pontos as $key => $value){
                if($valor['inicio'] == $value['id']){
                    $links[$indice]['inicio'] = $key;
                } else if($valor['fim'] == $value['id']){
                    $links[$indice]['fim'] = $key;
                }
            }
        }

        $diagrama = new LimparTabela('tbl_MapaAtos', '');
        $diagrama = new CadastrarDiagrama($pontos, $links);

        break;

    case "CarregarDiagrama":

        $diagrama = new BuscarDiagrama;
        $conjunto = array();

        if($diagrama->getAtos()){
            array_push($conjunto, array($diagrama->getAtos(), $diagrama->getLinks()));
            $conjunto = json_encode($conjunto);
            echo $conjunto;
        } else {
            return 0;
        }




        break;

    case "ContruirAto":

        $_SESSION['nomeAto'] = $_REQUEST['nomeAto'];

        $_SESSION['categorias'] = new SelecionarView('ViewCategoriasDisponiveisModulo', null);
        $_SESSION['categorias'] = $_SESSION['categorias']->getDados();

        $_SESSION['servicos'] = new SelecionarView('ViewModulosDisponiveis', null);
        $_SESSION['servicos'] = $_SESSION['servicos']->getDados();
        foreach($_SESSION['servicos'] as $chave => $valor){
            $_SESSION['servicos'][$chave]['selecionado'] = 0;
        }

        $retorno = "<ul class='collection'><li class='collection-item avatar'>";
        $retorno .= "<i class='material-icons circle blue darken-1'>verified_user</i><span class='title'>Conteúdo deste ato</span><div class='row'><div class='col-md-8' id='servicos-selecionados'>";


        $conteudoAto = new ListarConteudoAto($_SESSION['nomeAto']);

        foreach($conteudoAto->getResult() as $key => $value){
            foreach($_SESSION['servicos'] as $chave => $valor){
                if($value['idServico'] == $valor['id']){
                    $_SESSION['servicos'][$chave]['selecionado'] = 1;
                    $retorno .= "<div class='chip'>".$valor['strServico']."<i class='material-icons'>close</i></div>";
                }
            }
        }

        $retorno .= "</div><div class='col-md-4'><div><button id='cadastrarServicosAto' class='btn btn-info pull-right'>Cadastrar conteúdo</button></div><p><input type='checkbox' class='filled-in' id='checkValorUnico' checked='checked' /><label for='checkValorUnico'>Não permitir múltiplos</label></p></div></div></li><li class='collection-item avatar'>";
        $retorno .= "<i class='material-icons circle green darken-1'>extension</i><span class='title'>Pacotes disponíveis</span>";

        $retorno .= "<ul class='collapsible construindoAto' data-collapsible='expandable'>";

        foreach($_SESSION['categorias'] as $key => $value){
            $retorno .= "<li><div class='collapsible-header active'><i class='material-icons'>".$value['strNomeIcone']."</i>".$value['strCategoria']."</div>";
            $retorno .= "<div class='collapsible-body'><div class='row'>";

            foreach($_SESSION['servicos'] as $chave => $valor){
                if($valor['idCategoria'] == $value['id']){
                    $retorno .= "<div class='col s4'><div class='card blue-grey lighten-4'><div class='card-content white-text'>";
                    $retorno .= "<span class='card-title' style='text-weight: bold; color: #546e7a;'>".$valor['strServico']."</span>";
                    $retorno .= "<p style='padding: 0px; color: #546e7a;'>Adicionar resumo</p></div><div class='card-action'>";
                    $retorno .= "<a id=".$valor['id'].">Incluir</a>";
                    $retorno .= "</div></div></div>";
                }
            }

            $retorno .= "</div></div></li>";
        }
        $retorno .= "</ul></li></ul>";

        echo $retorno;

        break;

    case "AdicionarServico":
        foreach($_SESSION['servicos'] as $chave => $valor){
            if(($_REQUEST['idServico'] == $valor['id']) && ($_SESSION['servicos'][$chave]['selecionado'] == 0)){
                $_SESSION['servicos'][$chave]['selecionado'] = 1;
                echo "<div class='chip'>".$valor['strServico']."<i class='material-icons'>close</i></div>";
                exit();
            }
        }
        break;

    case "CadastrarConteudoAto":

        $limparTabela = new LimparTabela('rel_MapaAtos', ' WHERE nomeAto = '.$_SESSION['nomeAto']);

        $idAto = new SelectGenerico("tbl_MapaAtos", "WHERE strNomeAto = '".$_SESSION['nomeAto']."';");

        $multiplos = new AtualizarValorAtoUnico($idAto->getResult()[0]['id'], $_REQUEST['valorUnico']);

        foreach($_SESSION['servicos'] as $key => $value){
            if($value['selecionado'] == 1){
                $conteudo = new GravarServicoAto($_SESSION['nomeAto'], $value['id']);
                echo $conteudo->getResult();
            }
        }

        break;
}

?>
