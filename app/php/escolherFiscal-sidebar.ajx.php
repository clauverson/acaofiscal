<?php

if(!isset($_SESSION))
    session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch($acao){
    case "NovoFiscal":
        $Check = new Check;

        $cargo = $Check::Nome($_REQUEST['cargo_fiscal']);
        $matricula = $_REQUEST['matricula_fiscal'];
        $email = $_REQUEST['email_fiscal'];
        $cpf = $_REQUEST['cpf_fiscal'];
        $nome = $Check::Nome($_REQUEST['nome_fiscal']);

        if((!Check::Email($email)) || (!$Check::CPF($cpf))){
            echo 'Erro-Formulario'; exit();
        } else {
            $cpf = preg_replace("/\D+/", "", $cpf);
            $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        }

        $cadastrarFiscal = new CadastrarFiscal($nome, $cpf, $email, $cargo, $matricula);
        echo $cadastrarFiscal->NovoFiscal();

        break;
}

?>
