<?php

if(!session_id()){
    session_start();
}

include_once('../../../core/Config.inc.php');

$Documento = new BuscarDadosDocumentos('tbl_Documentos', 'AIIM');

if(count($Documento->getDados())){
    $caminho = RAIZ . "app/html/termos/aiim.html";
    $tpl = new Template($caminho);
    $tpl->tiafPenas = $Documento->getDados()[0]['txtPenas'];
    $tpl->tiafPrazo = $Documento->getDados()[0]['txtPrazo'];
    $tpl->tiafObservacoes = $Documento->getDados()[0]['txtObservacoes'];

    $tpl->nomeBotao = "Atualizar Termo";
    $tpl->show();
} else {
    $caminho = RAIZ . "app/html/termos/aiim.html";
    $tpl = new Template($caminho);

    $tpl->nomeBotao = "Cadastrar Termo";
    $tpl->show();
}

?>
