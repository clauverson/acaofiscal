<?php

class CriarTolerancia extends Create{

    private $idAcao;
    private $Result;

    function __construct($idAcao){
        self::setIdAcao($idAcao);
        self::NovaTolerancia();
    }

    private NovaTolerancia(){
        $dados = ['idAcaoFiscal' => self::getIdAcao()];
        parent::ExeCreate('tbl_Tolerancia', $dados);
        self::setResult(parent::getResult());
    }

    function getResult() {
        return $this->Result;
    }

    function getIdAcao() {
        return $this->idAcao;
    }

    function setIdAcao($idAcao) {
        $this->idAcao = $idAcao;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }

}

?>
