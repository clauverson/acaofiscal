$(document).ready(function(){

    $('#controle-sequenciarAtos').click(function(){
        $('#conteudo-gestor').load("app/php/controle-estruturaAtos.php");
    });

    var $menu = $('#ldd_menu');

    $menu.children('li').each(function(){
        var $this = $(this);
        var $span = $this.children('span');
        $span.data('width',$span.width());

        $this.bind('mouseenter',function(){
            $menu.find('.ldd_submenu').stop(true,true).hide();
            $span.stop().animate({'width':'25em'},300,function(){
                $this.find('.ldd_submenu').slideDown(300);
            });
        }).bind('mouseleave',function(){
            $this.find('.ldd_submenu').stop(true,true).hide();
            $span.stop().animate({'width':'10em'},300);
        });
    });

});
