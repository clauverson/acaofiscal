<?php

if(!session_id()){
    session_start();
}

include_once('../../core/Config.inc.php');

$dadosAcao = new BuscarDadosAcao($_SESSION['idAcao']);
$dadosAcao = $dadosAcao->getResult();

$dadosTermo = new BuscarDadosDocumentos('tbl_Documentos', $_REQUEST['tipoDocumento'], $_SESSION['idAcao']);
$dadosTermo = $dadosTermo->getDados();

switch($_REQUEST['tipoDocumento']){
    case "TIAF":
        $caminho = RAIZ . "app/html/termos/imprimir_tiaf.html";
        $tpl = new Template($caminho);

        $tpl->imprimirTiaf_razaoSocial = $dadosAcao[0]['strRazaoSocial'];
        $tpl->imprimirTiaf_cmc = $dadosAcao[0]['strCMC'];
        $tpl->imprimirTiaf_cnpj = $dadosAcao[0]['CNPJ'];
        $tpl->imprimirTiaf_atividade = $dadosAcao[0]['strAtividade'];
        $tpl->imprimirTiaf_endereco = $dadosAcao[0]['strLogradouro'].", ".$dadosAcao[0]['strNumero'].", ".$dadosAcao[0]['strBairro']." - ".$dadosAcao[0]['strCidade'];

        $tpl->imprimirTiaf_autuante = $dadosAcao[0]['strNomeFiscal'];
        $tpl->imprimirTiaf_RE = $dadosAcao[0]['RE'];
        $tpl->imprimirTiaf_cargo = $dadosAcao[0]['strCargo'];
        $tpl->imprimirTiaf_localData = "Ubatuba - ".date("d/m/Y");

        $tpl->imprimirTiaf_objetivo = $dadosTermo[0]['txtObjetivo'];
        $tpl->imprimirTiaf_notificacao = $dadosTermo[0]['txtNotificacao'];
        $tpl->imprimirTiaf_regimeEspecial = $dadosTermo[0]['txtRegimeEspecialFiscalizacao'];
        $tpl->imprimirTiaf_penas = $dadosTermo[0]['txtPenas'];
        $tpl->imprimirTiaf_prazos = $dadosTermo[0]['txtPrazo'];
        $tpl->imprimirTiaf_observacoes = $dadosTermo[0]['txtObservacoes'];

        $tpl->show();
        break;

    case "TD":

        break;
}

?>
