$(document).ready(function(){

	$('.collapsible').collapsible({
		accordion : false
	});

	$('.selec-part li').click(function(){
		if(!$(this).hasClass('fiscal-resp')){
			if($(this).hasClass('part-select')){
				$(this).removeClass('part-select');
			} else {
				$(this).addClass('part-select');
			}
		}
		listarSelecionados();
	});

	$('.select-fiscal li').click(function(){
		$('li').removeClass('fiscal-resp');
		$ref = $(this).attr('id');
		$('.id'+$ref).addClass('fiscal-resp');
		$('.id'+$ref).removeClass('part-select');
		listarSelecionados();
	});

	function listarSelecionados(){
		var texto = "";
		$('#caixa-participantes').html("");

		var idResp = $('.fiscal-resp').attr('id');
		var resp = $('.select-fiscal #'+idResp+' > span').text();
		var part = $('.part-select > span').text();
		if(resp){
			texto += "<span class='label label-danger' id='fiscal-responsavel'>"+resp+"</span>";
		}

		if(part){

			$('.part-select > span').each(function(){
				part = $(this).text();
				var id = $(this).parent('.part-select').attr('id');
				texto += "<span id="+id+" class='fiscal-participante'>"+part+"</span>";
			});
		}
		$('#caixa-participantes').append(texto);
	}

	$('#cadastrar_fiscal').click(function(){
		if($.fn.validar('#novo-fiscal .fnc-validar')){
			var dados = $('.novo-fiscal').serialize();
			dados += "&acao=NovoFiscal";

			$.ajax({
				method: "POST",
				url: "app/php/escolherFiscal-sidebar.ajx.php",
				datatype: 'json',
				data: dados,
				success: function(retorno){
					if(retorno == 'Erro-Formulario'){
						$.fn.mensagem('warning', 'Seu cadastro possui dados inválidos. Corrija as informações e tente novamente.');
					} else {
						if(retorno){
							if(retorno == 'ErroCpf-Existe'){
								$.fn.mensagem('warning', 'Este CPF já está em uso. Informe um novo CPF ou entre em contato com o suporte.');
							} else {
								$.fn.mensagem('success', 'Cadastro Concluído.');
								$('#conteudo-sidebar').load("app/php/escolherFiscal-sidebar.php");
							}
						} else {
							$.fn.mensagem('error', 'Não foi possível concluir este cadastro. Tente novamente mais tarde ou entre em contato com o suporte');
						}
					}
				}
			});

		} else {
			$.fn.mensagem('error', 'Há campos obrigatórios que não foram preenchidos');
		}

	});

});
