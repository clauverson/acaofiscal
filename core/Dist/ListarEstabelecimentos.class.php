<?php

class ListarEstabelecimentos extends Read{
    private $Locais;

    function __construct(){
        parent::ExeRead('tbl_Estabelecimento');
        $this->Locais = parent::getResult();
    }

    function getEstabelecimento(){
        return $this->Locais;
    }
}

?>
