$(document).ready(function(){

    $.fn.termos('CadastrarTermo', 'TIAF', 'CadastrarDocumento');

    $('.removeItem').click(function(){
        var div = $(this).parent('div');
        div.remove();
    });

    $('.edit').editable('app/php/jquery.jeditable.ajx.php', {
        indicator : 'Salvando...',
        tooltip   : 'Clique para editar',
        cancel    : 'Cancelar',
        submit    : 'OK',
        name      : 'valor'
    });

    $('#add-docSolicitado').click(function(){
        var dados = "acao=NovoItemDuplo&dados=" +$('#card-DocSolicitado').html();
        Editar('card-DocSolicitado', dados);
    });

    function Editar(local, dados){
        $.ajax({
            method: "POST",
            url: "app/php/appendItemEditable.ajx.php",
            datatype: 'json',
            data: dados,
            success: function(retorno){
                $('#' + local).html(retorno);
                $('.edit').editable('app/php/jquery.jeditable.ajx.php', {
                    indicator : 'Salvando...',
                    tooltip   : 'Clique para editar',
                    cancel    : 'Cancelar',
                    submit    : 'OK',
                    name      : 'valor'
                });
                $('.removeItem').click(function(){
                    var div = $(this).parent('div');
                    div.remove();
                });
            }
        });
    }

});
