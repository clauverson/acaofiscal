<?php

class SelectGenerico extends Read{

    private $Result;

    function __construct($tabela, $parametros){
        parent::ExeRead($tabela, $parametros);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->Result;
    }

    function setResult($dados){
        $this->Result = $dados;
    }

}

?>
