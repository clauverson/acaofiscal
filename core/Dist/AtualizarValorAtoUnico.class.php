<?php

class AtualizarValorAtoUnico extends Update{

    private $Result;

    function __construct($idAto, $valoUnico){
        if($valoUnico == 'true'){
            $valoUnico = true;
        } else {
            $valoUnico = false;
        }
        $dados = ['bUnico' => $valoUnico];
        parent::ExeUpdate("tbl_MapaAtos", $dados, "WHERE id = :idAto", "idAto=".$idAto);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }
}

?>
