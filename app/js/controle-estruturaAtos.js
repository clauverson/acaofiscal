$(document).ready(function(){
    var UltimoItem = null;

    //    INICIO - DIAGRAMA
    var graph = new joint.dia.Graph();

    var paper = new joint.dia.Paper({
        el: $('#papel-diagrama'),
        width: 1200,
        height: 600,
        gridSize: 1,
        model: graph
    });

    function state(x, y, label) {
        var cell = new joint.shapes.fsa.State({
            position: { x: x, y: y },
            size: { width: 60, height: 60 },
            attrs: {
                text : { text: label, fill: '#000000', 'font-weight': 'normal' },
                'circle': {
                    fill: '#f6f6f6',
                    stroke: '#000000',
                    'stroke-width': 2
                }
            }
        });
        graph.addCell(cell);
        return cell;
    }

    function link(source, target, label, vertices) {
        var cell = new joint.shapes.fsa.Arrow({
            source: { id: source.id },
            target: { id: target.id },
            labels: [{ position: 0.5, attrs: { text: { text: label || '', 'font-weight': 'bold' } } }],
            vertices: vertices || []
        });
        graph.addCell(cell);
        return cell;
    }

    paper.on('cell:pointerdblclick', function(cellView, evt, x, y) {

        var nomeAto = cellView.model.attr('text/text');
        $.fn.exibirModal();

        var dados = "acao=ContruirAto&nomeAto="+nomeAto;
        $.ajax({
            method: "POST",
            url: "app/php/controle-estruturaAtos.ajx.php",
            datatype: 'json',
            data: dados,
            success: function(retorno){
                $('#conteudo-modal').html(retorno);

                $('.collapsible').collapsible();

                $('.construindoAto a').click(function(){
                    var dados = "acao=AdicionarServico&idServico="+$(this).attr('id');
                    $.ajax({
                        method: "POST",
                        url: "app/php/controle-estruturaAtos.ajx.php",
                        datatype: 'json',
                        data: dados,
                        success: function(retorno){
                            $('#servicos-selecionados').append(retorno);
                        }
                    });
                });

                $('#cadastrarServicosAto').click(function(){
                    var valorUnico;
                    if($('#checkValorUnico').is(":checked")){
                        valorUnico = true;
                    }else{
                        valorUnico = false;
                    }
                    var dados = "acao=CadastrarConteudoAto&valorUnico="+valorUnico;
                    $.ajax({
                        method: "POST",
                        url: "app/php/controle-estruturaAtos.ajx.php",
                        datatype: 'json',
                        data: dados,
                        success: function(retorno){
                            $.fn.mensagem('success', 'Documento atualizado.');
                            $.fn.exibirModal();
                        }
                    });
                });
            }
        });
    });

    paper.on('cell:pointerclick', function(cellView, evt, x, y) {
        $('#input-nomeAto').val(cellView.model.attr('text/text'));
        UltimoItem = cellView;
    });

    carregarDiagrama();


    // BOTOES

    $('#novoAto').click(function(){
        var nome = $('#input-nomeAto').val();
        if(nome == ''){
            nome = "Novo";
        }
        var novo = state(340, 220, nome);

        UltimoItem = graph.getLastCell();

        resetarSelect();
    });

    function resetarSelect(){
        $('#selecionar-ato').html('');
        var itens = graph.getElements();
        $.each(itens, function(index, value){
            var text = value.attr('text/text');
            if(index != 0){
                $('#selecionar-ato').append('<option value='+value.id+'>'+text+'</option>');
            } else {
                $('#selecionar-ato').append('<option value='+value.id+'>Inicio</option>');
            }
        });
    }

    $('#ato-criarLink').click(function(){
        var primeiro = UltimoItem.model;
        var segundo = graph.getCell($('#selecionar-ato').val());

        link(primeiro,  segundo, 'opcional');
    });

    $('#ato-alterarNome').click(function(){
        UltimoItem.model.attr('text/text', $('#input-nomeAto').val());
        resetarSelect();
    });

    $('#CadastrarMapaAto').click(function(){
        var itens = JSON.stringify(graph.getCells());
        var dados = "acao=CadastrarDiagrama&dados="+itens;

        $.ajax({
            method: "POST",
            url: "app/php/controle-estruturaAtos.ajx.php",
            datatype: 'json',
            data: dados,
            success: function(retorno){
                $.fn.mensagem('success', 'Documento atualizado.');
            }
        });
    });

    function carregarDiagrama(){
        $.ajax({
            method: "POST",
            url: "app/php/controle-estruturaAtos.ajx.php",
            datatype: 'json',
            data: "acao=CarregarDiagrama",
            success: function(retorno){
                if(retorno != 0){
                    var itens = JSON.parse(retorno);

                    var atos = itens[0][0];
                    var links = itens[0][1];
                    var referencia = [];

                    $.each(atos,function(chave, valor){
                        if(valor['strNomeAto'] == "start"){
                            var start = new joint.shapes.fsa.StartState({ position: { x: valor['intPosX'], y: valor['intPosY'] } });
                            graph.addCell(start);
                            referencia.push(valor['id']);
                            referencia[valor['id']] = start;
                        } else {
                            var novo = state(valor['intPosX'], valor['intPosY'], valor['strNomeAto']);
                            referencia.push(valor['id']);
                            referencia[valor['id']] = novo;
                        }
                    });

                    $.each(links, function(chave, valor){
                        link(referencia[valor['idInicio']],  referencia[valor['idFim']], 'opcional');
                    });
                    resetarSelect();
                } else {
                    var start = new joint.shapes.fsa.StartState({ position: { x: 15 , y: 15 } });
                    graph.addCell(start);
                }

            }
        });
    }

    $('#limparDiagrama').click(function(){
        graph.clear();
    });

});
