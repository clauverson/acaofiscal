$(document).ready(function(){

    $('ul.tabs').tabs();

    $('#doc-tiaf').load("app/php/termos/tiaf.php");

    $('#li-tiaf').click(function(){
        $('#doc-tiaf').load("app/php/termos/tiaf.php");
    });

    $('#li-td').click(function(){
        $('#doc-td').load("app/php/termos/td.php");
    });

    $('#li-aiim').click(function(){
        $('#doc-aiim').load("app/php/termos/aiim.php");
    });

    $('#li-tif').click(function(){
        $('#doc-tif').load("app/php/termos/tif.php");
    });

});
