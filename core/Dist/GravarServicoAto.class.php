<?php

class GravarServicoAto extends Create{

    private $result;

    function __construct($nomeAto, $idServico){
        parent::ExeCreate('rel_MapaAtos', ['nomeAto' => $nomeAto, 'idServico' => $idServico]);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }
}

?>
