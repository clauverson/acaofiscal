<?php

class Conn{

    private static $Host = HOST;
    private static $User = USER;
    private static $Pass = PASS;
    private static $Dbsa = DBSA;

    private static $Connect = null;

    /**
     *Classe responsavel pela conexao ao banco de dados
     * @return [Conexao] [Verifica se já existe uma conexão, se não existir retorna uma conexao]
     */
    private static function Conectar(){
        try{
            if(self::$Connect == null){
                $dsn = 'mysql:host='.self::$Host.';dbname='.self::$Dbsa;
                $options = [ PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'];
                self::$Connect = new PDO($dsn, self::$User, self::$Pass, $options);
            }
        }catch (PDOException $e){

            PHPErro($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
            die;
        }

        self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$Connect;
    }

    /**
     * Classe responsavel por chamar uma nova conexao
     * @return [Conexao] [Metodo seguro de iniciar uma conexao]
     */
    public static function getConn(){
        return self::Conectar();
    }

}

?>
