$(document).ready(function(){

    $('#visualizarImpressao').click(function(){
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById('imprimirTIAF').innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    });

});
