<?php

class SelecionarView extends Read{
    private $Dados;

    function __construct($nomeView, $idAcao){
        if($idAcao){
            parent::FullRead("SELECT * FROM ".$nomeView." WHERE idAcao = ".$idAcao.";");
        } else {
            parent::FullRead("SELECT * FROM ".$nomeView.";");
        }

        self::setDados(parent::getResult());
    }

    function getDados(){
        return $this->Dados;
    }

    function setDados($dados){
        $this->Dados = $dados;
    }
}

?>
