<?php

if (!isset($_SESSION))
    session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch ($acao) {
    case "NovoItem":

        $atual = $_REQUEST['dados'];
        $novo = $atual . "<div class='row' style='margin-bottom: 0.6em;'><div class=' edit col-md-11'></div><i class='material-icons removeItem'>close</i></div>";

        echo $novo;

        break;

    case "NovoItemDuplo":

        $atual = $_REQUEST['dados'];
        $novo = $atual . "<div class='row col-md-6' style='margin-bottom: 0.6em;'><div class='edit col-md-11'></div><i class='material-icons removeItem'>close</i></div>";

        echo $novo;

        break;
}
?>
