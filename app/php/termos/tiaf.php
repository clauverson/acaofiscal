<?php

if(!session_id()){
    session_start();
}

include_once('../../../core/Config.inc.php');

function ListarItens($tabela, $colunas){
    $dados = new BuscarDocumentosSolicitados($tabela, null);
    $itens = "";

    if($dados->getResult()){
        foreach($dados->getResult() as $key => $value){
            if($colunas == 1){
                $itens .= "<div><div class='edit col-md-11'>".$value['strDescricao']."</div><i class='material-icons removeItem'>close</i></div>";
            } else {
                $itens .= "<div class='col-md-6'><div class='edit col-md-11'>".$value['strDescricao']."</div><i class='material-icons removeItem'>close</i></div>";
            }
        }
    }

    return $itens;
}

$Documento = new BuscarDadosDocumentos('tbl_Documentos', 'TIAF');

if(count($Documento->getDados())){

    $caminho = RAIZ . "app/html/card-termos.html";
    $tplDocSolicitado = new Template($caminho);
    $tplDocSolicitado->cardTitulo = "Intimação";
    $tplDocSolicitado->cardID = "card-DocSolicitado";
    $tplDocSolicitado->cardADDitem = "add-docSolicitado";
    $item = ListarItens("aux_docSolicitado", 2);
    $tplDocSolicitado->cardItens = $item;

    $caminho = RAIZ . "app/html/termos/tiaf.html";
    $tpl = new Template($caminho);
    $tpl->tiafObjetivo = $Documento->getDados()[0]['txtObjetivo'];
    $tpl->cardDocSolicitado = $tplDocSolicitado->pegar();
    $tpl->tiafRegimeEspecial = $Documento->getDados()[0]['txtRegimeEspecialFiscalizacao'];
    $tpl->tiafPenas = $Documento->getDados()[0]['txtPenas'];
    $tpl->tiafPrazo = $Documento->getDados()[0]['txtPrazo'];
    $tpl->tiafObservacoes = $Documento->getDados()[0]['txtObservacoes'];

    $tpl->nomeBotao = "Atualizar Termo";

    if(isset($_REQUEST['acao']) && ($_REQUEST['acao'] == 'novoTIAF')){
        $tpl->nomeBotao = "Incluir Termo";
        $_SESSION['local'] = 'novo-TIAF';
    } else {
        $_SESSION['local'] = 'atualizar-TIAF';
        $tpl->nomeBotao = "Atualizar Termo";
    }

    $tpl->show();
} else {
    $caminho = RAIZ . "app/html/card-termos.html";
    $tplDocSolicitado = new Template($caminho);
    $tplDocSolicitado->cardTitulo = "Intimação";
    $tplDocSolicitado->cardID = "card-DocSolicitado";
    $tplDocSolicitado->cardADDitem = "add-docSolicitado";

    $caminho = RAIZ . "app/html/termos/tiaf.html";
    $tpl = new Template($caminho);
    $tpl->cardDocSolicitado = $tplDocSolicitado->pegar();
    $tpl->nomeBotao = "Atualizar Termo";
    $tpl->show();
}

?>
