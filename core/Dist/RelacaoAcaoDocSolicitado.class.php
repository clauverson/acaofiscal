<?php

class RelacaoAcaoDocSolicitado extends Create{

    private $result;

    function __construct($idAcao, $idDocSolicitado, $intAtivo){
        parent::ExeCreate('AcaoFiscal_DocumentosSolicitados', ['idAcaoFiscal' => $idAcao, 'idDocumentoSolicitado' => $idDocSolicitado, 'intSituacao' => $intAtivo]);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }

}

?>
