<?php

if (!isset($_SESSION))
    session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch ($acao) {
    case "verificarDocumentosSolicitados":
        $preencherAnteriores = new SelecionarDocsAnteriores($_SESSION['idAcao']);

        $item = "";
        foreach($preencherAnteriores->getResult() as $key => $value){
            if($value['intSituacao'] == 1){
                echo "<form class='ativos'><div class='col-md-9' style='margin-bottom: 0.6em; padding: 0px;'><div class='edit col-md-11'>".$value['strDescricaoDocumento']."</div><i class='material-icons removeItem'>close</i></div><div class='switch col-md-3' style='margin:0px; padding: 0px'><label><input type='checkbox' name='apresentado' checked='true'><span class='lever' style='margin:0px; margin-right: 6px;'></span>Apresentado</label></div></form>";
            } else {
                echo "<form class='ativos'><div class='col-md-9' style='margin-bottom: 0.6em; padding: 0px;'><div class='edit col-md-11'>".$value['strDescricaoDocumento']."</div><i class='material-icons removeItem'>close</i></div><div class='switch col-md-3' style='margin:0px; padding: 0px'><label><input type='checkbox' name='apresentado'><span class='lever' style='margin:0px; margin-right: 6px;'></span>Apresentado</label></div></form>";
            }

        }

        break;

    case "carregarDocumentosSolicitados":

        $caminho = RAIZ . "app/html/card-termos.html";
        $tplDocSolicitado = new Template($caminho);

        $tplDocSolicitado->cardTitulo = "Documentos Solicitados";
        $tplDocSolicitado->cardID = "card-docSolicitado";
        $tplDocSolicitado->cardADDitem = "add-docSolicitado";

        echo $tplDocSolicitado->pegar();

        break;

    case "enviarDocumentosSolicitados":

        $relacaoDocumentos = array();
        $itens = explode("<>",$_REQUEST['lista']);

        foreach($itens as $key){
            $valores = explode("|", $key);
            if($valores[0] != ""){
                $valores[0] = 1;
            } else {
                $valores[0] = 0;
            }

            $buscaDoc = new BuscarDocumentoSolicitado($valores[1]);
            if($buscaDoc->getResult() == null){
                $novoDoc = new GravarDocSolicitado($valores[1]);
                array_push($relacaoDocumentos, array($novoDoc->getResult(), $valores[0]));
            } else {
                array_push($relacaoDocumentos, array($buscaDoc->getResult()[0]['id'], $valores[0]));
            }
        }

        $removerDocs = new RemoverDocsSolicitados($_SESSION['idAcao']);

        foreach($relacaoDocumentos as $key => $value){
            $relacao = new RelacaoAcaoDocSolicitado($_SESSION['idAcao'], $value[0], $value[1]);
        }

        break;
}

?>
