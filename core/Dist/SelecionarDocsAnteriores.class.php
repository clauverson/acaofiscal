<?php

class SelecionarDocsAnteriores extends Read{

    private $result;

    function __construct($idAcao){
        parent::FullRead("SELECT docs.strDescricaoDocumento, relacao.intSituacao FROM AcaoFiscal_DocumentosSolicitados relacao INNER JOIN tbl_Documentos_solicitados docs ON docs.id = relacao.idDocumentoSolicitado WHERE relacao.idAcaoFiscal = ".$idAcao);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }

}

?>
