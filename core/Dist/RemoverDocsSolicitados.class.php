<?php

class RemoverDocsSolicitados extends Delete{

    private $result;

    function __construct($idAcao){
        parent::ExeDelete('AcaoFiscal_DocumentosSolicitados', 'WHERE idAcaoFiscal = :acaofiscal' ,'acaofiscal='.$idAcao);
        self::setResult(parent::getResult());
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }
}

?>
