<?php

class CadastrarFiscal extends Create{
    private $Nome;
    private $Cpf;
    private $Email;
    private $Cargo;
    private $Matricula;
    private $Result;

    function __construct($Nome, $Cpf, $Email, $Cargo, $Matricula){
        self::setNome($Nome);
        self::setCpf($Cpf);
        self::setEmail($Email);
        self::setCargo($Cargo);
        self::setMatricula($Matricula);
    }

    function NovoFiscal(){
        $dados = ['strNomeFiscal' => self::getNome(), 'RE' => self::getMatricula(), 'strCargo' => self::getCargo(), 'strEmail' => self::getEmail(), 'strCPF' => self::getCpf()];
        self::setResult(parent::ExeCreate('tbl_Fiscal', $dados));
    }

    function setNome($Nome){
        $this->Nome = $Nome;
    }

    function setCpf($Cpf){
        $this->Cpf = $Cpf;
    }

    function setEmail($Email){
        $this->Email = $Email;
    }

    function setCargo($Cargo){
        $this->Cargo = $Cargo;
    }

    function setMatricula($Matricula){
        $this->Matricula = $Matricula;
    }

    function setResult($Result){
        $this->Result = $Result;
    }

    function getResult(){
        return $this->Result;
    }

    function getNome(){
        return $this->Nome;
    }
    function getCpf(){
        return $this->Cpf;
    }
    function getEmail(){
        return $this->Email;
    }
    function getCargo(){
        return $this->Cargo;
    }
    function getMatricula(){
        return $this->Matricula;
    }

}

?>
