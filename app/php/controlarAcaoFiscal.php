<?php

if(!session_id()){
    session_start();
}

include_once('../../core/Config.inc.php');

$caminho = RAIZ . "app/html/controlarAcaoFiscal.html";

$tpl = new Template($caminho);

$_SESSION['idAcao'] = $_REQUEST['idAcao'];
$dadosAcao = new BuscarDadosAcao($_REQUEST['idAcao']);

$numeroAcao = $dadosAcao->getResult()[0]['intNumeroAcaoFiscal'] .' / '. $dadosAcao->getResult()[0]['intAnoAcaoFiscal'];

$tpl->NomeFantasia = $dadosAcao->getResult()[0]['strNomeFantasia'];
$tpl->FiscalResponsavel = $dadosAcao->getResult()[0]['strNomeFiscal'];
$tpl->Logradouro = $dadosAcao->getResult()[0]['strLogradouro'];
$tpl->NumeroLocal = $dadosAcao->getResult()[0]['strNumero'];
$tpl->Complemento = '('.$dadosAcao->getResult()[0]['strComplemento'].')';
$tpl->Bairro = $dadosAcao->getResult()[0]['strBairro'];
$tpl->Cidade = $dadosAcao->getResult()[0]['strCidade'];
$tpl->PrazoAcaoFiscal = $dadosAcao->getResult()[0]['intPrazo'];

$tpl->CNPJ = $dadosAcao->getResult()[0]['CNPJ'];
$tpl->RazaoSocial = $dadosAcao->getResult()[0]['strRazaoSocial'];
$tpl->CMC = $dadosAcao->getResult()[0]['strCMC'];
$tpl->Atividade = $dadosAcao->getResult()[0]['strAtividade'];
$tpl->NumeroProcesso = $dadosAcao->getResult()[0]['strProcessoInicial'];
$numeroCompletoAcao = $dadosAcao->getResult()[0]['chSigla'].' '. $numeroAcao;
$tpl->NumeroAcaoFiscalCompleto = $numeroCompletoAcao;

if($dadosAcao->getResult()[0]['ISS'] == '1'){
    $iss = "Sim";
} else {
    $iss = "Não";
}
if($dadosAcao->getResult()[0]['ICMS'] == '1'){
    $icms = "Sim";
} else {
    $icms = "Não";
}
$tpl->ISS = $iss;
$tpl->ICMS = $icms;

$data = $dadosAcao->getResult()[0]['dtInicioAcao'];
$data = implode("/",array_reverse(explode("-",$data)));
$tpl->InicioAcao = $data;
$tpl->InicioPeriodo = $dadosAcao->getResult()[0]['dtInicioPeriodo'];
$tpl->FimPeriodo = $dadosAcao->getResult()[0]['dtFimPeriodo'];
$tpl->MotivacaoPrincipal = $dadosAcao->getResult()[0]['strMotivacaoPrincipal'];
$data = $dadosAcao->getResult()[0]['dtDiaCienciaInicio'];
$data = implode("/",array_reverse(explode("-",$data)));
$tpl->DataCiencia = $data;
$tpl->HoraCiencia = $dadosAcao->getResult()[0]['tmHoraCienciaInicio'];

$tpl->show();

?>
