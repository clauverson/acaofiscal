<?php

class BuscarPrimeiroAto extends Read{

    private $Result;

    function __construct(){
        parent::ExeRead('tbl_MapaAtos', "WHERE strNomeAto = 'start';");
        $idStart = parent::getResult()[0]['id'];

        parent::ExeRead('tbl_MapaAtos', "WHERE idInicio = :idStart", "idStart=".$idStart);
        $atos = parent::getResult();

        $disponiveis = array();

        foreach($atos as $key => $value){
            parent::ExeRead('tbl_MapaAtos', "WHERE id = :atoAtual", 'atoAtual='.$value['idFim']);
            $resultado = parent::getResult()[0];
            array_push($disponiveis, array($resultado['strNomeAto'],$resultado['bUnico']));
        }

        self::setResult($disponiveis);

    }

    function getResult(){
        return $this->Result;
    }

    function setResult($dados){
        $this->Result = $dados;
    }

}


?>
