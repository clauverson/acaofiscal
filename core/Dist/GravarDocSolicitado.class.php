<?php

class GravarDocSolicitado extends Create{

    private $item;
    private $result;

    function __construct($item){
        self::setItem($item);
        self::gravarItem();
    }

    private function gravarItem(){
        parent::ExeCreate('tbl_Documentos_solicitados', ['strDescricaoDocumento' => self::getItem()]);
        self::setResult(parent::getResult());
    }

    function getItem(){
        return $this->item;
    }

    function getResult(){
        return $this->result;
    }

    function setResult($result){
        $this->result = $result;
    }

    function setItem($item){
        $this->item = $item;
    }

}

?>
