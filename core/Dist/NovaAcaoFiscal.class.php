<?php

class NovaAcaoFiscal extends Create{

    private $icms;
    private $iss;
    private $dataInicioAcao;
    private $prazoDias;
    private $dataFinal;
    private $inicioPeriodo;
    private $fimPeriodo;
    private $motivacaoPrincipal;
    private $observacao;
    private $nomeDocumentoInterno;
    private $numeroDocumentoInterno;
    private $processoInicial;
    private $dataCiencia;
    private $horaCiencia;
    private $idEstabelecimento;
    private $idParticipantes;

    private $CPFContato;
    private $NomeContato;
    private $TelefoneContato;
    private $EmailContato;

    private $Sigla;
    private $NumeroAcaoFiscal;
    private $AnoAcaoFiscal = '2016';

    private $Result;

    function __construct($Sigla, $icms, $iss, $dataInicioAcao, $prazoDias, $dataFinal, $inicioPeriodo, $fimPeriodo, $motivacaoPrincipal, $observacao, $nomeDocumentoInterno, $numeroDocumentoInterno, $processoInicial, $dataCiencia, $horaCiencia, $idEstabelecimento, $idParticipantes, $proximoNumero, $CPFContato, $NomeContato, $TelefoneContato, $EmailContato){
        self::setSigla($Sigla);
        self::setIcms($icms);
        self::setIss($iss);

        self::setDataInicioAcao($dataInicioAcao);
        self::setPrazoDias($prazoDias);
        self::setDataFinal($dataFinal);
        self::setInicioPeriodo($inicioPeriodo);
        self::setFimPeriodo($fimPeriodo);

        self::setMotivacaoPrincipal($motivacaoPrincipal);
        self::setObservacao($observacao);
        self::setNomeDocumentoInterno($nomeDocumentoInterno);
        self::setNumeroDocumentoInterno($numeroDocumentoInterno);
        self::setProcessoInicial($processoInicial);

        self::setDataCiencia($dataCiencia);
        self::setHoraCiencia($horaCiencia);
        self::setIdEstabelecimento($idEstabelecimento);
        self::setIdParticipantes($idParticipantes);
        self::setNumeroAcaoFiscal($proximoNumero);

        self::setCPFContato($CPFContato);
        self::setNomeContato($NomeContato);
        self::setTelefoneContato($TelefoneContato);
        self::setEmailContato($EmailContato);

        self::GravarAcaoFiscal();
    }

    private function GravarAcaoFiscal(){
        $dados = ['chSigla' => self::getSigla(), 'intNumeroAcaoFiscal' => self::getNumeroAcaoFiscal(), 'intAnoAcaoFiscal' => self::getAnoAcaoFiscal(), 'strProcessoInicial' => self::getProcessoInicial(), 'idFiscal' => self::getIdParticipantes()[0], 'idEmpresa' => self::getIdEstabelecimento(), 'ISS' => self::getIss(), 'ICMS' => self::getIcms(), 'intPrazo' => self::getPrazoDias(), 'dtInicioAcao' => self::getDataInicioAcao(), 'dtInicioPeriodo' => self::getInicioPeriodo(), 'dtFimPeriodo' => self::getFimPeriodo(), 'strMotivacaoPrincipal' => self::getMotivacaoPrincipal(), 'strNomeDocumentoInterno' => self::getNomeDocumentoInterno(), 'strNumeroDocumentoInterno' => self::getNumeroDocumentoInterno(), 'txtObservacao' => self::getObservacao(), 'dtDiaCienciaInicio' => self::getDataCiencia(), 'tmHoraCienciaInicio' => self::getHoraCiencia(), 'strCPFcontato' => self::getCPFContato(), 'strNomeContato' => self::getNomeContato(), 'strTelefoneContato' => self::getTelefoneContato(), 'strEmailContato' => self::getEmailContato()];

        parent::ExeCreate('tbl_AcaoFiscal', $dados);
        self::setResult(parent::getResult());
    }

    function getSigla() {
        return $this->Sigla;
    }

    function getAnoAcaoFiscal() {
        return $this->AnoAcaoFiscal;
    }

    function getNumeroAcaoFiscal() {
        return $this->NumeroAcaoFiscal;
    }

    function getIcms() {
        return $this->icms;
    }

    function getIss() {
        return $this->iss;
    }

    function getDataInicioAcao() {
        return $this->dataInicioAcao;
    }

    function getPrazoDias() {
        return $this->prazoDias;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getInicioPeriodo() {
        return $this->inicioPeriodo;
    }

    function getFimPeriodo() {
        return $this->fimPeriodo;
    }

    function getMotivacaoPrincipal() {
        return $this->motivacaoPrincipal;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getNomeDocumentoInterno() {
        return $this->nomeDocumentoInterno;
    }

    function getNumeroDocumentoInterno() {
        return $this->numeroDocumentoInterno;
    }

    function getProcessoInicial() {
        return $this->processoInicial;
    }

    function getDataCiencia() {
        return $this->dataCiencia;
    }

    function getHoraCiencia() {
        return $this->horaCiencia;
    }

    function getIdEstabelecimento() {
        return $this->idEstabelecimento;
    }

    function getIdParticipantes() {
        return $this->idParticipantes;
    }

    function getResult() {
        return $this->Result;
    }

    function setSigla($Sigla){
        $this->Sigla = $Sigla;
    }

    function setResult($result){
        $this->Result = $result;
    }

    function setNumeroAcaoFiscal($NumeroAcaoFiscal){
        $this->NumeroAcaoFiscal = $NumeroAcaoFiscal;
    }

    function setIcms($icms) {
        $this->icms = $icms;
    }

    function setIss($iss) {
        $this->iss = $iss;
    }

    function setDataInicioAcao($dataInicioAcao) {
        $this->dataInicioAcao = $dataInicioAcao;
    }

    function setPrazoDias($prazoDias) {
        $this->prazoDias = $prazoDias;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    function setInicioPeriodo($inicioPeriodo) {
        $this->inicioPeriodo = $inicioPeriodo;
    }

    function setFimPeriodo($fimPeriodo) {
        $this->fimPeriodo = $fimPeriodo;
    }

    function setMotivacaoPrincipal($motivacaoPrincipal) {
        $this->motivacaoPrincipal = $motivacaoPrincipal;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    function setNomeDocumentoInterno($nomeDocumentoInterno) {
        $this->nomeDocumentoInterno = $nomeDocumentoInterno;
    }

    function setNumeroDocumentoInterno($numeroDocumentoInterno) {
        $this->numeroDocumentoInterno = $numeroDocumentoInterno;
    }

    function setProcessoInicial($processoInicial) {
        $this->processoInicial = $processoInicial;
    }

    function setDataCiencia($dataCiencia) {
        $this->dataCiencia = $dataCiencia;
    }

    function setHoraCiencia($horaCiencia) {
        $this->horaCiencia = $horaCiencia;
    }

    function setIdEstabelecimento($idEstabelecimento) {
        $this->idEstabelecimento = $idEstabelecimento;
    }

    function setIdParticipantes($idParticipantes) {
        $this->idParticipantes = $idParticipantes;
    }

    function getCPFContato() {
        return $this->CPFContato;
    }

    function getNomeContato() {
        return $this->NomeContato;
    }

    function getTelefoneContato() {
        return $this->TelefoneContato;
    }

    function getEmailContato() {
        return $this->EmailContato;
    }

    function setCPFContato($CPFContato) {
        $this->CPFContato = $CPFContato;
    }

    function setNomeContato($NomeContato) {
        $this->NomeContato = $NomeContato;
    }

    function setTelefoneContato($TelefoneContato) {
        $this->TelefoneContato = $TelefoneContato;
    }

    function setEmailContato($EmailContato) {
        $this->EmailContato = $EmailContato;
    }

}

?>
