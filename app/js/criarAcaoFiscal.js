$(document).ready(function(){

    //I N I C I A L I Z A C A O

    $(".cnpj-mask").mask("99.999.999/9999-99");
    $(".cpf-mask").mask("999.999.999-99");

    $('.calendario').datetimepicker({
        viewMode: 'months',
        format: 'DD/MM/YYYY',
        locale: 'pt-br'
    });

    $('.horario').datetimepicker({
        format: 'LT',
        locale: 'pt-br'
    });

    $('.meses').datetimepicker({
        viewMode: 'years',
        format: 'MM/YYYY',
        locale: 'pt-br'
    });

    $('select').material_select();

    // S I D E B A R   -   F I S C A I S
    $('#escolherFiscal-sidebar').click(function(){
        $('#conteudo-sidebar').load("app/php/escolherFiscal-sidebar.php");
        UIkit.offcanvas.show('#sidebar');
    });

    // S I D E B A R  -  A U T U A D O S
    $('#escolherAutuado-sidebar').click(function(){
        $('#conteudo-sidebar').load("app/php/escolherAutuado-sidebar.php");
        UIkit.offcanvas.show('#sidebar');
    });

    // S O M A  R   D I A S   A   U M A   D A T A
    function somarDias(){
        var data = $('#calendario').val();
        var qtdDias = $('#prazo-dias').val();
        qtdDias = parseInt(qtdDias);
        data = data.split('/');
        newData = data[1]+'/'+data[0]+'/'+data[2];
        data = new Date(newData);
        data.setDate((data.getDate()+qtdDias));

        $('#data-final').html(data.getDate()+'/'+(data.getMonth()+1)+'/'+data.getFullYear());
    }
    $('#prazo-dias').change(function(){
        if($('#calendario').val() != ''){
            somarDias();
        }
    });

    $('#calendario').blur(function(){
        if($('#prazo-dias').val() != ''){
            somarDias();
        }
    });

    // C A D A S T R A R   A C A O   F I S C A L
    $('#CadastrarAcaoFiscal').click(function(){
        var dados = $('#form-acaoFiscal').serialize();
        var part = [];
        $('#caixa-participantes > span').each(function(){
            part.push($(this).text());
        });

        if(part.length != 0){
            dados += '&participantes='+part;
            dados += '&data-final='+$('#data-final').text();
            dados += '&id-estabelecimento='+$('#estabelecimento-escolhido > span').attr('id');
            dados += '&acao=NovaAcao';

            $.ajax({
                method: "POST",
                url: "app/php/criarAcaoFiscal.ajx.php",
                datatype: 'json',
                data: dados,
                success: function(retorno){
                    $.fn.mensagem('success', 'Nova ação cadastrada');
                    $('#conteudoPainel').load("app/php/criarAcaoFiscal.php");
                }
            });
        } else {

        }

    });
});
