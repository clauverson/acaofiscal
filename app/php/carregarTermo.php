<?php

if(!session_id()){
    session_start();
}

unset($_SESSION['idTermo']);

include_once('../../../core/Config.inc.php');

$caminho = RAIZ . "app/html/termos/tiaf.html";
$tpl = new Template($caminho);

$Documento = new BuscarDadosDocumentos('tbl_Documentos', 'TIAF');

if(count($Documento->getDados())){
    $_SESSION['idTermo'] = $Documento->getDados()[0]['id'];
    $tpl->tiafObjetivo = $Documento->getDados()[0]['txtObjetivo'];
    $tpl->tiafNotificacao = $Documento->getDados()[0]['txtNotificacao'];
    $tpl->tiafRegimeEspecial = $Documento->getDados()[0]['txtRegimeEspecialFiscalizacao'];
    $tpl->tiafPenas = $Documento->getDados()[0]['txtPenas'];
    $tpl->tiafPrazo = $Documento->getDados()[0]['txtPrazo'];
    $tpl->tiafObservacoes = $Documento->getDados()[0]['txtObservacoes'];
}

$tpl->nomeBotao = "Atualizar Termo";

if(isset($_REQUEST['acao']) && ($_REQUEST['acao'] == 'novoTIAF')){
    $tpl->nomeBotao = "Incluir Termo";
    $_SESSION['local'] = 'novo-TIAF';
} else {
    $_SESSION['local'] = 'atualizar-TIAF';
    $tpl->nomeBotao = "Atualizar Termo";
}

$tpl->show();

?>
