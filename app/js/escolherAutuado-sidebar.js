$(document).ready(function(){

    $(".cnpj-mask").mask("99.999.999/9999-99");

    $('.collapsible').collapsible({
        accordion : false
    });

    $('#tabEstabelecimento li a').click(function () {
        var classe = $(this).attr('id');
        $('div .tab-pane').removeClass('active');
        $('.' + classe).addClass('active');
    });

    $('#pular-tab').click(function(){
        $('div.tab-pane').removeClass('active');
        $('#tabEstabelecimento #tab-dados').parent('li').removeClass('active');
        $('#tabEstabelecimento #tab-endereco').parent('li').addClass('active');
        $('div.tab-endereco').addClass('active');
    });

    $('#escolherAvatar li').click(function(){
        $id = $(this).attr('id');
        $nomeLocal = $(this).find('span').text();
        $cnpj = $(this).find('p').text();
        $span = "<span class='label label-warning' id="+$id+">"+$nomeLocal+"</span><span>CNPJ: "+$cnpj+"</span>";

        $('#estabelecimento-escolhido').html($span);
    });

    $('#cadastrar_estabelecimento').click(function(){
        if($.fn.validar('#form_estabelecimento .fnc-validar')){
            var dados = $('#form_estabelecimento input').serialize();
            dados += "&acao=NovoEstabelecimento";

            $.ajax({
                method: "POST",
                url: "app/php/escolherAutuado-sidebar.ajx.php",
                datatype: 'json',
                data: dados,
                success: function(retorno){
                    $.fn.mensagem('success', 'Cadastro Concluído.');
                }
            });
        } else {
            $.fn.mensagem('error', 'Há campos obrigatórios que não foram preenchidos');
        }
    });

});
