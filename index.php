<?php

if(!session_id()){
    session_start();
}

include('core/Config.inc.php');

$login = new Login(1);

$tpl = new Template("app/html/index.html");
$msg = '';

$dataLogin = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if(!empty($dataLogin['AdminLogin'])){
    $login->ExeLogin($dataLogin);
    if(!$login->getResult()){
        $msg = "<script>$(document).ready(function(){
	Lobibox.notify('{$login->getError()[1]}', {
		size: 'mini',
		position: 'bottom right',
		width: 460,
		icons: false,
		msg: '{$login->getError()[0]}'
	});
});</script>";
    } else {
        header('Location: painel.php');
    }
}


$tpl->msg = $msg;
$tpl->show();
?>
