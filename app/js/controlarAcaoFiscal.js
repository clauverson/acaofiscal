$(document).ready(function(){

    $("#fileuploader").uploadFile({
        url:"YOUR_FILE_UPLOAD_URL",
        fileName:"myfile"
    });

    // carregar lista de atos disponíveis
    $.ajax({
        method: "POST",
        url: "app/php/controlarAcaoFiscal.ajx.php",
        datatype: 'json',
        data: "acao=listarDocumentos",
        success: function(retorno){
            $('#tabela-processo').html(retorno);
            $('a.secondary-content').click(function(){
                $nomeArquivo = $(this).attr('id');
                $('#conteudoPainel').load("app/php/termos/"+$nomeArquivo.toLowerCase()+".php?acao=novo");
            });
            $('.acessar-termo').click(function(){
                $indiceAto = $(this).attr('idAto');
                $('#conteudoPainel').load("app/php/acompanharTermo.php?indiceAto="+$indiceAto);
            });
        }
    });

    $('#entregarDocumento').click(function(){
        $('#baseDireita').load("app/php/entregarDocumento.php");
    });


});
