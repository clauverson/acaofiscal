<?php

if(!session_id()){
	session_start();
}

include_once('../../core/Config.inc.php');

$caminho = RAIZ . "app/html/escolherFiscal-sidebar.html";
$tpl = new Template($caminho);

$listaFiscais = new ListarFiscais;

$caminho = RAIZ . "app/html/avatarFiscais.html";
$templateAvatar = new Template($caminho);

$avatar = new MontarListaFiscais($listaFiscais->getFiscais() ,$templateAvatar);
$listaCompleta = "<ul class=\"collection\" id='escolherAvatar'>";
$listaCompleta .= $avatar->getResult();
$listaCompleta .= "</ul>";

$tpl->TodosFiscais = $listaCompleta;
$tpl->FiscalResponsavel = $listaCompleta;
$tpl->show();

?>
