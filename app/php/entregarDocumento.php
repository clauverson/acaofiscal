<?php

if(!session_id()){
    session_start();
}

unset($_SESSION['idTermo']);

include_once('../../core/Config.inc.php');

$documentosSolicitados = new BuscarDocumentosSolicitados("ViewDocumentosPendentes", $_SESSION['idAcao']);
$documentosSolicitados = $documentosSolicitados->getResult();
//echo"<pre>";
//var_dump($documentosSolicitados);
//echo"</pre>";

$listaDocs = "<ul class='collection'>";
foreach($documentosSolicitados as $key => $value){
    $listaDocs .= "<li class='collection-item'>{$value['strDescricao']}</li>";
}
$listaDocs .= "</ul>";

$caminho = RAIZ . "app/html/entregarDocumento.html";
$tpl = new Template($caminho);
$tpl->listaDocsSolicitados = $listaDocs;
$tpl->show();

?>
