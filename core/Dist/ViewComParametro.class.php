<?php

class ViewComParametro extends Read{
    private $Dados;

    function __construct($nomeView, $parametro){
        parent::ExeRead($nomeView, $parametro);

        self::setDados(parent::getResult());
    }

    function getDados(){
        return $this->Dados;
    }

    function setDados($dados){
        $this->Dados = $dados;
    }
}

?>
