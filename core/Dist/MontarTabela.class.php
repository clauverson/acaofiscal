<?php

class MontarTabela{

    private $Tabela;
    private $Dados;

    function __construct($tipoTabela, $dados){
        self::setDados($dados);

        switch($tipoTabela){
            case "AcoesAbertas":
                self::AcoesAbertas();
                break;

            case "DocumentosDisponiveis":
                self::DocumentosDisponiveis();
                break;
        }
    }

    private function AcoesAbertas(){
        $AcoesAbertas = self::getDados();

        if(sizeof($AcoesAbertas) > 0){
            $tabela = "<table class='striped responsive-table'><thead><tr>";
            foreach($AcoesAbertas[0] as $coluna => $value){
                $tabela .= "<th>".$coluna."</th>";
            }
            $tabela .= "</tr></thead><tbody>";

            foreach($AcoesAbertas as $chave){
                $tabela .= "<tr>";
                foreach($chave as $coluna => $valor){
                    $tabela .= "<td>".$valor."</td>";
                }
                $tabela .= "<td style='text-align: center;'><a id=".$chave['#']." class='waves-effect waves-light btn btnAcessarAcoesAbertas'><i class='material-icons right'>send</i>Acessar</a><td></tr>";
            }
            $tabela .= "</tbody></table>";
        } else {
            $tabela = "<table class='striped responsive-table'><thead><tr>";
            $tabela .= "<th>Não há ações abertas</th>";
            $tabela .= "</tr></thead><tbody>";

        }

        self::setTabela($tabela);
    }

    private function DocumentosDisponiveis(){
        $ListaDocumentos = self::getDados();
        $tabela = "";

        if(sizeof($ListaDocumentos) > 0){
            $tabela .= "<table class='striped responsive-table'><thead><tr>";
            $tabela .= "<th>#</th>";

            foreach($ListaDocumentos[0] as $coluna => $value){
                if($coluna != 'idAcao'){
                    $tabela .= "<th>".$coluna."</th>";
                }

            }
            $tabela .= "</tr></thead><tbody>";

            $contador = 0;
            $final = "";
            foreach($ListaDocumentos as $key => $value){
                $idAto = $key;
                $tabela .= "<tr>";
                $tabela .= "<td>".($contador+=1)."</td>";
                foreach($value as $coluna => $valor){
                    if($coluna != 'idAcao'){
                        $tabela .= "<td>".$valor."</td>";
                    } else {
                        $tabela .= "<td class='hidden idAcao'>".$valor."</td>";
                    }
                }
                $tabela .= "<td><a class='waves-effect waves-light btn acessar-termo' idAto=".$idAto.">Acessar</a></td></tr>";
            }

            $tabela .= "</tbody></table>";

            $tabela .= "<br><ul class='collection with-header'><li class='collection-header'><h5>Atos Disponíveis</h5></li>";

            if(sizeof($ListaDocumentos) > 0){
                foreach($ListaDocumentos as $key => $value){
                    $nomeAto = $value['Ato'];

                    $ramificacoes = new BuscarRamificacoes($nomeAto);
                    foreach($ramificacoes->getResult() as $chave => $valor){
                        $tabela .= "<li class='collection-item'><div>".$valor[0]."<a id='".$valor[0]."' class='secondary-content'><i class='material-icons'>send</i></a></div></li>";
                    }
                }
            }
        }

        if(sizeof($ListaDocumentos) == 0){
            $tabela .= "<br><ul class='collection with-header'><li class='collection-header'><h5>Bem-vindo, escolha uma Ato para começarmos</h5></li>";
        }

        $primeiroAto = new BuscarPrimeiroAto();
        $primeiroAto = $primeiroAto->getResult();

        foreach($primeiroAto as $chave => $valor){
            $ativo = new SelectGenerico("rel_Acao_Ato", "WHERE nomeAto = '".$valor[0]."' AND idAcaoFiscal = ".$_SESSION['idAcao']);

            if($valor[1] == '1'){
                $valor[1] = true;
            } else {
                $valor[1] = false;
            }

            if(!($valor[1]) || (sizeof($ativo->getResult()) == 0)){
                $tabela .= "<li class='collection-item'><div>".$valor[0]."<a id='".$valor[0]."' class='secondary-content'><i class='material-icons'>send</i></a></div></li>";
            }
        }

        $tabela .= "</ul>";

        self::setTabela($tabela);
    }

    function getTabela(){
        return $this->Tabela;
    }

    function getDados(){
        return $this->Dados;
    }

    function setDados($dados){
        $this->Dados = $dados;
    }

    function setTabela($tabela){
        $this->Tabela = $tabela;
    }

}

?>
