<?php

class BuscarDadosAcao extends Read{
	private $Result;

	function __construct($idAcao){
		self::dadosAcaoFiscal($idAcao);
	}

	private function dadosAcaoFiscal($idAcao){
		parent::FullRead("SELECT acao.id as 'id_Acao', acao.chSigla, acao.intNumeroAcaoFiscal, acao.intAnoAcaoFiscal, acao.strProcessoInicial, acao.idFiscal, acao.idEmpresa, acao.ISS, acao.ICMS, acao.intPrazo, acao.dtInicioAcao, acao.dtInicioPeriodo, acao.dtFimPeriodo, acao.strMotivacaoPrincipal, acao.strNomeDocumentoInterno, acao.strNumeroDocumentoInterno, acao.txtObservacao, acao.dtDiaCienciaInicio, acao.tmHoraCienciaInicio, acao.strCPFcontato, acao.strNomeContato, acao.strTelefoneContato, acao.strEmailContato, acao.dbFaturamentoEsperadoJan, acao.dbIndiceOcupacao, acao.dbValorMaximo, acao.dbValorBaixo, fiscal.id as 'id_Fiscal', fiscal.strCPF, fiscal.strNomeFiscal, fiscal.RE, fiscal.strCargo, fiscal.strEmail, fiscal.strLogin, fiscal.strSenha, fiscal.intNivelAcesso, fiscal.strApelido, empresa.id as 'id_Empresa', empresa.CNPJ, empresa.strRazaoSocial, empresa.strNomeFantasia, empresa.strCMC, empresa.strAtividade, empresa.strLogradouro, empresa.strNumero, empresa.strComplemento, empresa.strBairro, empresa.strCidade FROM tbl_AcaoFiscal acao INNER JOIN tbl_Estabelecimento empresa ON empresa.id = acao.idEmpresa INNER JOIN tbl_Fiscal fiscal ON fiscal.id = acao.idFiscal WHERE acao.id = ".$idAcao." LIMIT 1;");
		self::setResult(parent::getResult());
	}

	function getResult() {
		return $this->Result;
	}

	function setResult($result) {
		$this->Result = $result;
	}

}

?>
