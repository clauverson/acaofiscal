<?php

class NovoDocumento extends Create{

    private $Tabela;
    private $NomeDocumento;
    private $Result;

    function __construct($Tabela, $NomeDocumento){
        self::setTabela($Tabela);
        self::setNomeDocumento($NomeDocumento);
        self::Criar();
    }

    private function Criar(){
        $dados = ['strNomeDoc' => self::getNomeDocumento()];
        parent::ExeCreate(self::getTabela(), $dados);
        self::setResult(parent::getResult());
    }

    function getTabela() {
        return $this->Tabela;
    }

    function getNomeDocumento() {
        return $this->NomeDocumento;
    }

    function getResult() {
        return $this->Result;
    }

    function setResult($Result) {
        $this->Result = $Result;
    }

    function setTabela($Tabela) {
        $this->Tabela = $Tabela;
    }

    function setNomeDocumento($NomeDocumento) {
        $this->NomeDocumento = $NomeDocumento;
    }

}

?>
