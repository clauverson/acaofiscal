<?php

if(!isset($_SESSION))
    session_start();

addslashes($_REQUEST);
addslashes($_GET);
addslashes($_POST);

include_once('../../core/Config.inc.php');

$acao = $_REQUEST['acao'];

switch($acao){
    case "listarDocumentos":

        $acoes = new SelecionarView('viewDocsIniciados', $_SESSION['idAcao']);

        $tabela = new MontarTabela('DocumentosDisponiveis', $acoes->getDados());
        echo $tabela->getTabela();

        break;

    case "buscarPrimeiroAto":

        $primeiroAto = new BuscarPrimeiroAto();

        var_dump($primeiroAto->getResult());

        break;

}

?>
