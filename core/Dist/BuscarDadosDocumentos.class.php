<?php

class BuscarDadosDocumentos extends Read{
    private $Dados;

    function __construct($Tabela, $nomeTermo, $idAcao = null){
        self::BuscarDados($Tabela, $nomeTermo, $idAcao);
    }

    private function BuscarDados($NomeTabela, $nomeTermo, $idAcao){
        if($idAcao){
            parent::ExeRead($NomeTabela, "WHERE strNomeDoc = :termo AND idAcaoFiscal = :acaofiscal order by id desc Limit 1" ,"acaofiscal=".$idAcao."&termo=".$nomeTermo);
        } else {
            parent::ExeRead($NomeTabela, "WHERE strNomeDoc = :termo AND idAcaoFiscal is null order by id desc Limit 1", "termo=".$nomeTermo);
        }

        self::setDados(parent::getResult());
    }

    function getDados() {
        return $this->Dados;
    }

    function setDados($Dados) {
        $this->Dados = $Dados;
    }
}

?>
