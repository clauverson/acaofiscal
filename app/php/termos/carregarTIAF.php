<?php

if(!session_id()){
    session_start();
}

include_once('../../../core/Config.inc.php');

$caminho = RAIZ . "app/html/termos/carregarTIAF.html";

$Documento = new BuscarDadosDocumentos('doc_TIAF');

$tpl = new Template($caminho);

$tpl->tiafObjetivo = $Documento->getDados()[0]['txtObjetivo'];
$tpl->tiafNotificacao = $Documento->getDados()[0]['txtNotificacao'];
$tpl->tiafRegimeEspecial = $Documento->getDados()[0]['txtRegimeEspecialFiscalizacao'];
$tpl->tiafPenas = $Documento->getDados()[0]['txtPenas'];
$tpl->tiafPrazo = $Documento->getDados()[0]['txtPrazo'];
$tpl->tiafObservacoes = $Documento->getDados()[0]['txtObservacoes'];

$tpl->show();

?>
