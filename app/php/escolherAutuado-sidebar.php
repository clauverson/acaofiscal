<?php

if(!session_id()){
    session_start();
}

include_once('../../core/Config.inc.php');

$caminho = RAIZ . "app/html/escolherAutuado-sidebar.html";
$tpl = new Template($caminho);

$listaLocais = new ListarEstabelecimentos;

$caminho = RAIZ . "app/html/listaEstabelecimento.html";
$templateLista = new Template($caminho);

$itens = new MontarListaEstabelecimentos($listaLocais->getEstabelecimento() ,$templateLista);

$listaCompleta = "<ul class=\"collection\" id='escolherAvatar'>";
$listaCompleta .= $itens->getResult();
$listaCompleta .= "</ul>";

$tpl->EstabelecimentosCadastrados = $listaCompleta;

$tpl->show();

?>
