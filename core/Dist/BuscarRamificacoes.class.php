<?php

class BuscarRamificacoes extends Read{

    private $Result;

    function __construct($nomeAto){

        $valorStatus = new SelectGenerico("rel_Acao_Ato", "WHERE nomeAto = '".$nomeAto."' AND idAcaoFiscal = ".$_SESSION['idAcao']);
        $valorStatus = $valorStatus->getResult()[0]['intStatus'];

        parent::ExeRead('tbl_MapaAtos', "WHERE strNomeAto = :nomeAto;", "nomeAto=".$nomeAto);
        $idAtoAtual = parent::getResult()[0]['id'];

        parent::ExeRead('tbl_MapaAtos', "WHERE idInicio = :idAto", "idAto=".$idAtoAtual);
        $atos = parent::getResult();

        $disponiveis = array();

        foreach($atos as $key => $value){
            if($value['intRequisitos'] <= $valorStatus){
                parent::ExeRead('tbl_MapaAtos', "WHERE id = :atoAtual", 'atoAtual='.$value['idFim']);
                $resultado = parent::getResult()[0];
                array_push($disponiveis, array($resultado['strNomeAto'], $value['intRequisitos']));
            }
        }

        self::setResult($disponiveis);
    }

    function getResult(){
        return $this->Result;
    }

    function setResult($dados){
        $this->Result = $dados;
    }

}

?>
